const helpers={};

helpers.isAuthenticated =(req, res, next)=>{
    if(req.isAuthenticated()){
        return next();
    }else{
        req.flash('error_msg','No eres administrador');
        res.redirect('/forum');
    }
}
module.exports = helpers;