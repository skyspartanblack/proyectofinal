const express = require('express');
const router = express.Router();
const {isAuthenticated} = require('../helpers/auth')

const usersController = require('../controllers/usersController');

router.get('/users',isAuthenticated, usersController.listUsers);
router.post('/adduser',isAuthenticated, usersController.saveUser);
router.post('/usersearch',isAuthenticated, usersController.search);
router.get('/deleteuser/:idusuario',isAuthenticated,usersController.deleteUser);
router.get('/updateuser/:idusuario',isAuthenticated, usersController.editUser);
router.post('/updateuser/:idusuario',isAuthenticated,usersController.updateUser);

module.exports = router;