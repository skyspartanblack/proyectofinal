const express = require('express');
const path = require('path');
const morgan = require('morgan');
const mysql = require ('mysql');
const session = require ('express-session');
const myConnection = require('express-myconnection');
const flash = require('connect-flash');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const passportLocal = require('passport-local').Strategy;
const MySqlStore = require('express-mysql-session')(session);
const dotenv = require('dotenv');
const app = express();

//dotenv path config
dotenv.config({ 
    path: path.resolve(__dirname, './env/.env') 
 })
const connection = require('../database/db');

//importando las rutas
const usersRoute = require('./routes/users');
const noticiasRoute = require('./routes/news');
const forumRoute = require('./routes/forum');



//opciones
app.set('view engine','ejs');
app.set('views', path.join(__dirname,'views'));


//middlewares
app.use(morgan('dev'));
app.use(myConnection(mysql,{
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    port:3306,
    database: process.env.DB_DATABASE
},'single'));
app.use(express.urlencoded({extended:false}));

//configuracion para guardar la sessiones
let options ={
    host:process.env.DB_HOST,
    port: 3306,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database:process.env.DB_DATABASE
};
app.use(cookieParser());
var poolCoonect = mysql.createPool(options);
var StoreSess = new MySqlStore(options,poolCoonect);
app.use(session({
    secret:"secret",
    store: StoreSess,
    resave: false,
    saveUninitialized: false,
}));
//configuracion para guardar las sessiones

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//variables globales para mostrar mensajes flash
app.use(function(req, res, next){
    app.locals.success_msg = req.flash("success_msg");
    app.locals.error_msg = req.flash("error_msg");
    app.locals.error = req.flash("error");
    app.locals.user = req.user || null;
    next();
  });

passport.use('local',new passportLocal({
    usernameField:'email',
    passwordField:'password',
    passReqToCallback:true
}, function(req, username, password, done){
    connection.query("select * from usuarios where correoE = ?",[username],function(err, results){
        if(err){
            return done(err);
        }
        if(!results.length){
            return done(null, false, req.flash('error_msg','Usuario no existente'))
        }
        if(!(results[0].contra == password)){
            return done(null, false,req.flash('error_msg','Contraseña incorrecta'))
        }
        return done(null,results[0]) 
    })
}));
//serializacion del usuarios
passport.serializeUser(function(user,done){
    done(null, user.idusuario);
});

//deserializacion del usuario
passport.deserializeUser(function(id,done){
    connection.query("select idusuario, correoE,nombre, rol from usuarios where idusuario = "+id,function(err,results){
        console.log(results);
        done(err,results[0]);
    })
})


//rutas
app.use('/',noticiasRoute);
app.use('/',forumRoute);
app.use('/',usersRoute);

//autenticar usuario con passport
app.post('/auth',passport.authenticate('local',{
    successRedirect:'/news',
    failureRedirect:'/login',
    failureFlash:true
}));
app.get('/logout',(req,res)=>{
    req.logOut();
    res.redirect('/login');
})
//render routes
app.get('/',(req, res)=>{
    res.render('index');
});
app.get('/news',(req,res)=>{
    res.render('news');
});
app.get('/users', (req,res)=>{
    res.render('users');
})
app.get('/forum',(req,res)=>{
    res.render('forum');
});
app.get('/login',(req, res)=>{
    res.render('login');
});

//static file
app.use(express.static(path.join(__dirname,'/public')));


//starting server
let port = process.env.PORT || 3000;
app.listen(port,()=>{
    //console.log('servidor iniciado en puerto 3000');
    //para heroku
    console.log('Servidor iniciado en puerto',port);
});
