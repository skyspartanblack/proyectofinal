const controller = {};

controller.search = (req, res)=>{
    const buscar = req.body.buscar;
    console.log(buscar)
    req.getConnection((err, conn)=>{
        conn.query("select * from noticias where titulo like ?",['%'+buscar+'%'],(err, noticia)=>{
            console.log(noticia);
            
            res.render('news',{
                
                data:noticia
                
            })
        })
    })
}
controller.listNews = (req, res)=>{
    req.getConnection((err,conn)=>
    {
        conn.query('select * from noticias',(err,noticias)=>{
            if(err){
                res.json(err);
            }
            res.render('news',{
                data:noticias
            })
        })
    })
};

controller.saveNews = (req,res)=>{
    const data = req.body;
    req.getConnection((err, conn)=>{
        conn.query('insert into noticias set ?',[data],(err, noticias)=>{
            console.log(noticias);
            req.flash('success_msg','Noticia Publicada');
            res.redirect('/news');
        })
    })
};

controller.editNews =(req,res)=>{
    const id = req.params.idnoticia;
    req.getConnection((err, conn)=>
    {
        conn.query('select * from noticias where idnoticia = ?',[id], (err, noticia)=>{
            res.render('news_edit',{
                data:noticia[0],
            });
        })
    })
}

controller.updateNew = (req,res)=>{
    const idnoticia = req.params.idnoticia;
    const titulo = req.body.titulo;
    const newNoticia = req.body;
    req.getConnection((err, conn)=>{
        conn.query('update noticias set ? where idnoticia = ?',[newNoticia, idnoticia],(err,rows)=>{
            req.flash('success_msg','Noticia: \"'+titulo+'\" editada correctamente');
            res.redirect('/news');
        })
    })
}

controller.deleteNew = (req, res)=>{
    const idnoticia = req.params.idnoticia;
    req.getConnection((err, conn)=>{
        conn.query('delete from noticias where idnoticia = ?',[idnoticia],(err, rows)=>{
            req.flash('success_msg','Noticia eliminada correctamente');
            res.redirect('/news');
        })
    })
}


module.exports = controller;