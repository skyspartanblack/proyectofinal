const controller = {}

controller.listForumNews = (req, res)=>{
    req.getConnection((err,conn)=>
    {        
        conn.query('select * from noticias',(err,noticias)=>{
            if(err){
                res.json(err);
            }
            console.log('noticias');
            conn.query('select * from noticias ORDER BY idnoticia DESC',(err,recientes)=>{
                if(err){
                    res.json(err);
                }
                console.log(recientes);
                res.render('forum',{ 
                    data:noticias,
                    recent: recientes
                });
            })         
        });
    });
};
module.exports = controller;